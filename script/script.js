

$(document).ready(function () {
    /*Our services*/
    $('.services-tab-links-list').click(function (event) {
        if ($(event.target).prop('tagName') === 'LI') {
            $(event.target).addClass('services-tab-title-active').siblings().removeClass('services-tab-title-active')
            $('.services-tab-list-content').find('.services-tab-content-item').removeClass('services-tab-content-item-active animate__fadeInLeft').eq($(event.target).index()).addClass('services-tab-content-item-active animate__fadeInLeft')
        } else {
            $(event.target).closest('li').addClass('services-tab-title-active').siblings().removeClass('services-tab-title-active')
            $('.services-tab-list-content').find('.services-tab-content-item').removeClass('services-tab-content-item-active animate__fadeInLeft').eq($(event.target).closest('li').index()).addClass('services-tab-content-item-active animate__fadeInLeft')
        }
    })

    /*Our Amazing Work*/
    let maxLoadImg = 12;
    let targetAttribute

    function showImages(image) {
        image.addClass('animate__animated animate__backInUp')
        return image.css('display', 'block')
    }

    function hideImages(image) {
        return image.css('display', 'none')

    }


    $('.our-works-img-item').each(function (index) {
        hideImages($(this))
        index < maxLoadImg ?
            showImages($(this)) : null
    })
    $('.our-works_menu-list').click(function (event) {
        $(event.target).addClass('our-works-menu-item-active').siblings().removeClass('our-works-menu-item-active')
        targetAttribute = $(event.target).data('our-works')
        $(this).parent().find('.our-works-img-item').each(function (index) {
            hideImages($(this))
            if ($(this).data('our-works') === targetAttribute) {
                index < maxLoadImg ?
                    showImages($(this)) : null
            }
            if (targetAttribute === 'all') {
                index < maxLoadImg ?
                    showImages($(this)) : null
            }
        })
    })
    /*Button load more*/
    $('#load-more-works')
        .click(function () {
            maxLoadImg += 12
            $(this).siblings('.loader').css('display', 'flex')
            setTimeout(() => {
                $(this).siblings('.loader').css('display', 'none')
                $(this).siblings('.our-works-img-list').children('.our-works-img-item').each(function (index) {
                    hideImages($(this))
                    if($('.our-works-menu-item-active').data('our-works') === 'all') {
                        index < maxLoadImg ?
                            showImages($(this)) : null
                    }
                    if (($(this).data('our-works')) === targetAttribute) {
                        console.log(targetAttribute)
                        index < maxLoadImg ?
                            showImages($(this)) : null
                    }
                })
            }, 2000)
            if (maxLoadImg === 36) {
                $(this).css('display', 'none')
            }
        })


    /*Testimonials*/
    $('#customers-testimonials').owlCarousel({
        loop: true,
        center: true,
        items: 1,
        autoplay: true,
        dots: true,
        nav: true,
        autoplayTimeout: 5500,
        smartSpeed: 450,
        dotsContainer: '.testimonials-next-container .testimonials-next-xs-avatar',
        navText: [$('.btn-left'), $('.btn-right')]
    })
});
$('.testimonials-next-xs-avatar').click(function () {
    $('.owl-carousel').trigger('to.owl.carousel', [$(this).index()]);

   /* $('.best-images-grid').masonry({
        itemSelector: '.images-grid-item',
        columnWidth: 378,
    });*/
})




